package ru.soft.malikov.web.stavforum;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NcfuStavForumApplication {

    public static void main(String[] args) {
        SpringApplication.run(NcfuStavForumApplication.class, args);
    }
}
