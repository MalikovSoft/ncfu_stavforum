package ru.soft.malikov.web.stavforum.reps;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import ru.soft.malikov.web.stavforum.model.Member;

import java.util.List;

public interface MembersRepository extends PagingAndSortingRepository<Member, Long> {

    /**
     * @param pageable - "Параметр постраничного вывода"
     * @return "Страница постраничного вывода"
     */
    Page<Member> findAll(Pageable pageable);

    /**
     * @return "Все участники мероприятия"
     */
    List<Member> findAll();

}
