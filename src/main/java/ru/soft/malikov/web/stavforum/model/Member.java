package ru.soft.malikov.web.stavforum.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Сущность участника форума "Всемирного Русского Народного Собора"
 *
 * @author malikovsoft
 * @version 1.0
 */

@Entity
@Table(name = "members", schema = "service", catalog = "")
public class Member {

    @Id
    @GeneratedValue
    private Long id;                    // Первичный ключ

    @Basic
    @Column(name = "numberOfInvitation")
    private Long numberOfInvitation;    // Номер приглашения

    @Basic
    @Column(name = "surname")
    private String surname;             //Фамилия

    @Basic
    @Column(name = "name")
    private String name;                // Имя

    @Basic
    @Column(name = "patronymic")
    private String patronymic;          // Отчество

    @Basic
    @Column(name = "coutry")
    private String country;             // Страна проживания

    @Basic
    @Column(name = "city")
    private String city;                // Населенный пункт

    @Basic
    @Column(name = "position")
    private String position;            // Должность

    @Basic
    @Column(name = "academicStatus")
    private String academicStatus;      // Ученое звание

    @Basic
    @Column(name = "academicDegree")
    private String academicDegree;      // Ученая степень

    @Basic
    @Column(name = "seriesOfPassport")
    private Long seriesOfPassport;      // Серия паспорта

    @Basic
    @Column(name = "numberOfPassport")
    private Long numberOfPassport;      // Номер паспорта

    @Basic
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "dateOfIssue")
    private Timestamp dateOfIssue;      // Дата выдачи

    @Basic
    @Column(name = "registeredAddress")
    private String registeredAddress;   // Адрес регистрации

    @Basic
    @Column(name = "email")
    private String email;               // Адрес электронной почты

    @Basic
    @Column(name = "phone")
    private String phone;               // Контактный телефон

    @Basic
    @Column(name = "events")
    private String events;              // Мероприятия

    @Basic
    @Column(name = "subjectOfSpeech")
    private String subjectOfSpeech;     // Тема выступления

    @Basic
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    @Column(name = "arrivalDate")
    private Timestamp arrivalDate;      // Дата и время прибытия

    @Basic
    @Column(name = "arrivalPlace")
    private String arrivalPlace;        // Место прибытия

    @Basic
    @Column(name = "hotelAccommodation")
    private Boolean hotelAccommodation; // Проживание в гостинице

    @Basic
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    @Column(name = "departureDate")
    private Timestamp departureDate;    // Дата и время отъезда

    @Basic
    @Column(name = "departurePlace")
    private String departurePlace;      // Место отъезда

    @Basic
    @Column(name = "otherInfo")
    private String otherInfo;           // Дополнительная информация

    public Member() {
    }


    /**
     * @param numberOfInvitation - "Номер приглашения"
     * @param surname            - "Фамилия"
     * @param name               - "Имя"
     * @param patronymic         - "Отчество"
     * @param country            - "Страна проживания"
     * @param city               - "Населенный пункт"
     * @param position           - "Должность"
     * @param academicStatus     - "Ученое звание"
     * @param academicDegree     - "Ученая степень"
     * @param seriesOfPassport   - "Серия паспорта"
     * @param numberOfPassport   - "Номер паспорта"
     * @param dateOfIssue        - "Дата выдачи паспорта"
     * @param registeredAddress  - "Адрес регистрации"
     * @param email              - "Электронная почта"
     * @param phone              - "Номер телефона"
     * @param events             - "Мероприятия"
     * @param subjectOfSpeech    - "Тема выступления"
     * @param arrivalDate        - "Дата и время прибытия"
     * @param arrivalPlace       - "Место прибытия"
     * @param hotelAccommodation - "Проживание в гостинице"
     * @param departureDate      - "Дата и время отъезда"
     * @param departurePlace     - "Место отъезда"
     * @param otherInfo          - "Дополнительная информация"
     */
    public Member(Long numberOfInvitation, String surname, String name, String patronymic, String country, String city, String position, String academicStatus, String academicDegree, Long seriesOfPassport, Long numberOfPassport, Timestamp dateOfIssue, String registeredAddress, String email, String phone, String events, String subjectOfSpeech, Timestamp arrivalDate, String arrivalPlace, Boolean hotelAccommodation, Timestamp departureDate, String departurePlace, String otherInfo) {
        setNumberOfInvitation(numberOfInvitation);
        setSurname(surname);
        setName(name);
        setPatronymic(patronymic);
        setCountry(country);
        setCity(city);
        setPosition(position);
        setAcademicStatus(academicStatus);
        setAcademicDegree(academicDegree);
        setSeriesOfPassport(seriesOfPassport);
        setNumberOfPassport(numberOfPassport);
        setDateOfIssue(dateOfIssue);
        setRegisteredAddress(registeredAddress);
        setEmail(email);
        setPhone(phone);
        setEvents(events);
        setSubjectOfSpeech(subjectOfSpeech);
        setArrivalDate(arrivalDate);
        setArrivalPlace(arrivalPlace);
        setHotelAccommodation(hotelAccommodation);
        setDepartureDate(departureDate);
        setDeparturePlace(departurePlace);
        setOtherInfo(otherInfo);
    }

    /**
     * @return "Идентификатор в БД"
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id - "Идентификатор в БД"
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return "Номер приглашения"
     */
    public Long getNumberOfInvitation() {
        return numberOfInvitation;
    }

    /**
     * @param numberOfInvitation - "Номер приглашения"
     */
    public void setNumberOfInvitation(Long numberOfInvitation) {
        this.numberOfInvitation = numberOfInvitation;
    }

    /**
     * @return "Фамилия"
     */
    public String getSurname() {
        return surname;
    }

    /**
     * @param surname - "Фамилия"
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * @return "Имя"
     */
    public String getName() {
        return name;
    }

    /**
     * @param name - "Имя"
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return "Отчество"
     */
    public String getPatronymic() {
        return patronymic;
    }

    /**
     * @param patronymic - "Отчество"
     */
    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    /**
     * @return "Страна проживания"
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country - "Страна проживания"
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return "Населенный пункт"
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city - "Населенный пункт"
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return "Должность"
     */
    public String getPosition() {
        return position;
    }

    /**
     * @param position - "Должность"
     */
    public void setPosition(String position) {
        this.position = position;
    }

    /**
     * @return "Ученое звание"
     */
    public String getAcademicStatus() {
        return academicStatus;
    }

    /**
     * @param academicStatus - "Ученое звание"
     */
    public void setAcademicStatus(String academicStatus) {
        this.academicStatus = academicStatus;
    }

    /**
     * @return "Ученая степень"
     */
    public String getAcademicDegree() {
        return academicDegree;
    }

    /**
     * @param academicDegree - "Ученая степень"
     */
    public void setAcademicDegree(String academicDegree) {
        this.academicDegree = academicDegree;
    }

    /**
     * @return "Серия паспорта"
     */
    public Long getSeriesOfPassport() {
        return seriesOfPassport;
    }

    /**
     * @param seriesOfPassport - "Серия паспорта"
     */
    public void setSeriesOfPassport(Long seriesOfPassport) {
        this.seriesOfPassport = seriesOfPassport;
    }

    /**
     * @return "Номер паспорта"
     */
    public Long getNumberOfPassport() {
        return numberOfPassport;
    }

    /**
     * @param numberOfPassport - "Номер паспорта"
     */
    public void setNumberOfPassport(Long numberOfPassport) {
        this.numberOfPassport = numberOfPassport;
    }

    /**
     * @return "Дата выдачи паспорта"
     */
    public Timestamp getDateOfIssue() {
        return dateOfIssue;
    }

    /**
     * @param dateOfIssue - "Дата выдачи паспорта"
     */
    public void setDateOfIssue(Timestamp dateOfIssue) {
        this.dateOfIssue = dateOfIssue;
    }

    /**
     * @return "Адрес регистрации"
     */
    public String getRegisteredAddress() {
        return registeredAddress;
    }

    /**
     * @param registeredAddress - "Адрес регистрации"
     */
    public void setRegisteredAddress(String registeredAddress) {
        this.registeredAddress = registeredAddress;
    }

    /**
     * @return "Электронная почта"
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email - "Электронная почта"
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return "Контактный телефон"
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone - "Контактный телефон"
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return "Мероприятия"
     */
    public String getEvents() {
        return events;
    }

    /**
     * @param events - "Мероприятия"
     */
    public void setEvents(String events) {
        this.events = events;
    }

    /**
     * @return "Тема выступления"
     */
    public String getSubjectOfSpeech() {
        return subjectOfSpeech;
    }

    /**
     * @param subjectOfSpeech - "Тема выступления"
     */
    public void setSubjectOfSpeech(String subjectOfSpeech) {
        this.subjectOfSpeech = subjectOfSpeech;
    }

    /**
     * @return "Дата и время прибытия"
     */
    public Timestamp getArrivalDate() {
        return arrivalDate;
    }

    /**
     * @param arrivalDate - "Дата и время прибытия"
     */
    public void setArrivalDate(Timestamp arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    /**
     * @return "Место прибытия"
     */
    public String getArrivalPlace() {
        return arrivalPlace;
    }

    /**
     * @param arrivalPlace - "Место прибытия"
     */
    public void setArrivalPlace(String arrivalPlace) {
        this.arrivalPlace = arrivalPlace;
    }

    /**
     * @return "Проживание в гостинице"
     */
    public Boolean getHotelAccommodation() {
        return hotelAccommodation;
    }

    /**
     * @param hotelAccommodation - "Проживание в гостинице"
     */
    public void setHotelAccommodation(Boolean hotelAccommodation) {
        this.hotelAccommodation = hotelAccommodation;
    }

    /**
     * @return "Дата отъезда"
     */
    public Timestamp getDepartureDate() {
        return departureDate;
    }

    /**
     * @param departureDate - "Дата и время отъезда"
     */
    public void setDepartureDate(Timestamp departureDate) {
        this.departureDate = departureDate;
    }

    /**
     * @return "Место отъезда"
     */
    public String getDeparturePlace() {
        return departurePlace;
    }

    /**
     * @param departurePlace - "Место отъезда"
     */
    public void setDeparturePlace(String departurePlace) {
        this.departurePlace = departurePlace;
    }

    /**
     * @return "Дополнительная информация"
     */
    public String getOtherInfo() {
        return otherInfo;
    }

    /**
     * @param otherInfo - "Дополнительная информация"
     */
    public void setOtherInfo(String otherInfo) {
        this.otherInfo = otherInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Member)) return false;
        Member member = (Member) o;
        return Objects.equals(getId(), member.getId()) &&
                Objects.equals(getNumberOfInvitation(), member.getNumberOfInvitation()) &&
                Objects.equals(getSurname(), member.getSurname()) &&
                Objects.equals(getName(), member.getName()) &&
                Objects.equals(getPatronymic(), member.getPatronymic()) &&
                Objects.equals(getCountry(), member.getCountry()) &&
                Objects.equals(getCity(), member.getCity()) &&
                Objects.equals(getPosition(), member.getPosition()) &&
                Objects.equals(getAcademicStatus(), member.getAcademicStatus()) &&
                Objects.equals(getAcademicDegree(), member.getAcademicDegree()) &&
                Objects.equals(getSeriesOfPassport(), member.getSeriesOfPassport()) &&
                Objects.equals(getNumberOfPassport(), member.getNumberOfPassport()) &&
                Objects.equals(getDateOfIssue(), member.getDateOfIssue()) &&
                Objects.equals(getRegisteredAddress(), member.getRegisteredAddress()) &&
                Objects.equals(getEmail(), member.getEmail()) &&
                Objects.equals(getPhone(), member.getPhone()) &&
                Objects.equals(getEvents(), member.getEvents()) &&
                Objects.equals(getSubjectOfSpeech(), member.getSubjectOfSpeech()) &&
                Objects.equals(getArrivalDate(), member.getArrivalDate()) &&
                Objects.equals(getArrivalPlace(), member.getArrivalPlace()) &&
                Objects.equals(getHotelAccommodation(), member.getHotelAccommodation()) &&
                Objects.equals(getDepartureDate(), member.getDepartureDate()) &&
                Objects.equals(getDeparturePlace(), member.getDeparturePlace()) &&
                Objects.equals(getOtherInfo(), member.getOtherInfo());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getNumberOfInvitation(), getSurname(), getName(), getPatronymic(), getCountry(), getCity(), getPosition(), getAcademicStatus(), getAcademicDegree(), getSeriesOfPassport(), getNumberOfPassport(), getDateOfIssue(), getRegisteredAddress(), getEmail(), getPhone(), getEvents(), getSubjectOfSpeech(), getArrivalDate(), getArrivalPlace(), getHotelAccommodation(), getDepartureDate(), getDeparturePlace(), getOtherInfo());
    }
}
