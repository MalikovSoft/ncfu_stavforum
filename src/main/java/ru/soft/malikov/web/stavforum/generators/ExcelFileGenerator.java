package ru.soft.malikov.web.stavforum.generators;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import ru.soft.malikov.web.stavforum.model.Member;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class ExcelFileGenerator {

    private HSSFWorkbook workbook;
    private HSSFSheet sheet;
    private List<Member> forumMembers;

    public ExcelFileGenerator() {
        workbook = new HSSFWorkbook();
        sheet = workbook.createSheet("Список участников форума");
        forumMembers = null;
    }

    public ExcelFileGenerator(List<Member> forumMembers) {
        workbook = new HSSFWorkbook();
        sheet = workbook.createSheet("Список участников форума");
        setForumMembers(forumMembers);
    }

    public void generateExcelFile(File outputFile) {

        String[] headers = {
                "ИД",
                "Номер приглашения",
                "Фамилия",
                "Имя",
                "Отчество",
                "Страна проживания",
                "Населенный пункт",
                "Должность",
                "Ученое звание",
                "Ученая степень",
                "Серия паспорта",
                "Номер паспорта",
                "Дата выдачи",
                "Адрес регистрации",
                "Адрес электронной почты",
                "Контактный телефон",
                "Мероприятия",
                "Тема выступления",
                "Дата и время прибытия",
                "Место прибытия",
                "Проживание в гостинице",
                "Дата и время отъезда",
                "Место отъезда",
                "Дополнительная информация"
        };

        createSheetHeader(headers);
        fillData();

        try (FileOutputStream out = new FileOutputStream(outputFile)) {
            workbook.write(out);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public List<Member> getForumMembers() {
        return forumMembers;
    }

    public void setForumMembers(List<Member> forumMembers) {
        this.forumMembers = forumMembers;
    }

    private void createSheetHeader(String[] headers) {
        Row row = sheet.createRow(0);
        Font font = workbook.createFont();
        font.setBold(true);
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setFont(font);
        for (int cellNumber = 0; cellNumber < headers.length; cellNumber++) {
            row.createCell(cellNumber).setCellValue(headers[cellNumber]);
            row.getCell(cellNumber).setCellStyle(cellStyle);
        }
    }

    private void fillData() {
        for (int rowNumber = 1; rowNumber == forumMembers.size(); rowNumber++) {
            Row row = sheet.createRow(rowNumber);
            Member memberOfForum = forumMembers.get(rowNumber - 1);
            row.createCell(0).setCellType(CellType.NUMERIC);
            row.createCell(0).setCellValue(memberOfForum.getId());
            row.createCell(1).setCellType(CellType.NUMERIC);
            row.createCell(1).setCellValue(memberOfForum.getNumberOfInvitation());
            row.createCell(2).setCellValue(memberOfForum.getSurname());
            row.createCell(3).setCellValue(memberOfForum.getName());
            row.createCell(4).setCellValue(memberOfForum.getPatronymic());
            row.createCell(5).setCellValue(memberOfForum.getCountry());
            row.createCell(6).setCellValue(memberOfForum.getCity());
            row.createCell(7).setCellValue(memberOfForum.getPosition());
            row.createCell(8).setCellValue(memberOfForum.getAcademicStatus());
            row.createCell(9).setCellValue(memberOfForum.getAcademicDegree());
            row.createCell(10).setCellValue(memberOfForum.getSeriesOfPassport());
            row.createCell(11).setCellValue(memberOfForum.getNumberOfPassport());
            row.createCell(12).setCellValue(memberOfForum.getDateOfIssue());
            row.createCell(13).setCellValue(memberOfForum.getRegisteredAddress());
            row.createCell(14).setCellValue(memberOfForum.getEmail());
            row.createCell(15).setCellValue(memberOfForum.getPhone());
            row.createCell(16).setCellValue(memberOfForum.getEvents());
            row.createCell(17).setCellValue(memberOfForum.getSubjectOfSpeech());
            row.createCell(18).setCellValue(memberOfForum.getArrivalDate());
            row.createCell(19).setCellValue(memberOfForum.getArrivalPlace());
            row.createCell(20).setCellType(CellType.BOOLEAN);
            row.createCell(20).setCellValue(memberOfForum.getHotelAccommodation());
            row.createCell(21).setCellValue(memberOfForum.getDepartureDate());
            row.createCell(22).setCellValue(memberOfForum.getDeparturePlace());
            row.createCell(23).setCellValue(memberOfForum.getOtherInfo());
        }
    }
}
