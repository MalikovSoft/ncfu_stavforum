package ru.soft.malikov.web.stavforum.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import ru.soft.malikov.web.stavforum.generators.ExcelFileGenerator;
import ru.soft.malikov.web.stavforum.model.Member;
import ru.soft.malikov.web.stavforum.reps.MembersRepository;

import java.io.File;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@RestController
@RequestMapping(path = "/api")
public class RestApiController {

    @Autowired
    private MembersRepository forumMembers;

    @Value("${upload.path}")
    private String uploadPath;

    @PostMapping(path = "/add_forum_member")
    public @ResponseBody
    String memberRegistration(
            @RequestParam long numberOfInvitation,
            @RequestParam String surname,
            @RequestParam String name,
            @RequestParam String patronimic,
            @RequestParam String country,
            @RequestParam String city,
            @RequestParam String position,
            @RequestParam String academicStatus,
            @RequestParam String academicDegree,
            @RequestParam long seriesOfPassport,
            @RequestParam long numberOfPassport,
            @RequestParam String dateOfIssue,
            @RequestParam String registeredAddress,
            @RequestParam String email,
            @RequestParam String phone,
            @RequestParam String events,
            @RequestParam String subjectOfSpeech,
            @RequestParam String arrivalDate,
            @RequestParam String arrivalPlace,
            @RequestParam boolean hotelAccommodation,
            @RequestParam String departureDate,
            @RequestParam String departurePlace,
            @RequestParam String otherInfo
    ) {
        DateFormat tmpDateOfIssueFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat tmpArrivalDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        DateFormat tmpDepartureDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        Date parseDateOfIssue = null;
        Date parseArrivalDate = null;
        Date parseDepartureDate = null;
        try {
            parseDateOfIssue = tmpDateOfIssueFormat.parse(dateOfIssue);
            parseArrivalDate = tmpArrivalDateFormat.parse(arrivalDate);
            parseDepartureDate = tmpDepartureDateFormat.parse(departureDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Timestamp resDateOfIssue = new Timestamp(parseDateOfIssue.getTime());
        Timestamp resArrivalDate = new Timestamp(parseArrivalDate.getTime());
        Timestamp resDepartureDate = new Timestamp(parseDepartureDate.getTime());

        Member forumMember = new Member();
        forumMember.setNumberOfInvitation(numberOfInvitation);
        forumMember.setSurname(surname);
        forumMember.setName(name);
        forumMember.setPatronymic(patronimic);
        forumMember.setCountry(country);
        forumMember.setCity(city);
        forumMember.setPosition(position);
        forumMember.setAcademicStatus(academicStatus);
        forumMember.setAcademicDegree(academicDegree);
        forumMember.setSeriesOfPassport(seriesOfPassport);
        forumMember.setNumberOfPassport(numberOfPassport);
        forumMember.setDateOfIssue(resDateOfIssue);
        forumMember.setRegisteredAddress(registeredAddress);
        forumMember.setEmail(email);
        forumMember.setPhone(phone);
        forumMember.setEvents(events);
        forumMember.setSubjectOfSpeech(subjectOfSpeech);
        forumMember.setArrivalDate(resArrivalDate);
        forumMember.setArrivalPlace(arrivalPlace);
        forumMember.setHotelAccommodation(hotelAccommodation);
        forumMember.setDepartureDate(resDepartureDate);
        forumMember.setDeparturePlace(departurePlace);
        forumMember.setOtherInfo(otherInfo);

        try {
            forumMembers.save(forumMember);
        } catch (Exception ex) {
            return "error";
        }

        return "saved";
    }

    @PostMapping(path = "/generate_excel_file")
    public @ResponseBody
    String generateExcelFile() {
        File outputDir = new File(uploadPath);
        if (!outputDir.exists()) {
            outputDir.mkdir();
        }
        String uuidFile = UUID.randomUUID().toString();
        String resultFilename = outputDir.getPath() + "/Forum_members_" + uuidFile + ".xls";
        ExcelFileGenerator excelCreator = new ExcelFileGenerator(forumMembers.findAll());
        excelCreator.generateExcelFile(new File(resultFilename));
        return "generated";
    }

    @PostMapping(path = "/generated_files_list")
    public @ResponseBody
    String generatedFilesList() {
        File outputDir = new File(uploadPath);
        if (!outputDir.exists()) {
            outputDir.mkdir();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        File[] files = outputDir.listFiles();
        for (int i = 0; i < files.length; i++) {
            sb.append(files[i].getName());
            if (i != files.length - 1) {
                sb.append(",");
            }
        }
        sb.append("]");

        return sb.toString();
    }
}
