package ru.soft.malikov.web.stavforum.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.soft.malikov.web.stavforum.model.Member;
import ru.soft.malikov.web.stavforum.reps.MembersRepository;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping(path = "/")
public class IndexController {

    @Autowired
    private ServletContext context;

    @Value("${upload.path}")
    private String uploadPath;

    @Autowired
    private MembersRepository forumMembers;

    @GetMapping(path = "/")
    public String toIndexPage(@PageableDefault(page = 0, size = 20) Pageable pageable, Model model) {

        Page<Member> membersOfForumPage = forumMembers.findAll(pageable);
        List<File> fileList = Arrays.asList(new File(uploadPath + '/').listFiles());
        model.addAttribute("convertedFiles", fileList);
        model.addAttribute("membersOfForumPage", membersOfForumPage);

        return "index";
    }


    @GetMapping(path = "/generated-files/{fileName:.+}")
    public void downloader(HttpServletRequest request, HttpServletResponse response,
                           @PathVariable("fileName") String fileName) {
        try {
            String downloadFolder = context.getRealPath(uploadPath);
            File file = new File(uploadPath + File.separator + fileName);

            if (file.exists()) {
                String mimeType = context.getMimeType(file.getPath());

                if (mimeType == null) {
                    mimeType = "application/octet-stream";
                }

                response.setContentType(mimeType);
                response.addHeader("Content-Disposition", "attachment; filename=" + fileName);
                response.setContentLength((int) file.length());

                OutputStream os = response.getOutputStream();
                FileInputStream fis = new FileInputStream(file);
                byte[] buffer = new byte[4096];
                int b = -1;

                while ((b = fis.read(buffer)) != -1) {
                    os.write(buffer, 0, b);
                }

                fis.close();
                os.close();
            } else {
                System.out.println("Requested " + fileName + " file not found!!");
            }
        } catch (IOException e) {
            System.out.println("Error:- " + e.getMessage());
        }
    }


}
