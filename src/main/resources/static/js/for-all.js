$(function () {
    var paginationSettings = {
        pages: $('#light-pagination').data('count'),
        prevText: 'Назад',
        nextText: 'Вперед',
        displayedPages: '20',
        currentPage: $('#light-pagination').data('current'),
        hrefTextPrefix: '?page=',
        hrefTextSuffix: '&size=',
        cssStyle: 'light-theme'
    };

    $('#light-pagination').pagination(paginationSettings);

    $('#generate-excel-btn').click(function () {
        $.post("/api/generate_excel_file", function (data) {
        });
    })
})